import { Component, OnInit, Input } from '@angular/core';
import { SignInComponent } from '../login/sign-in/sign-in.component';
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  SignInStatus: boolean;

  constructor(private router: Router) { }

  ngOnInit() {
    this.SignInStatus = true;
  }

  changeStatusToRegister(){
      this.SignInStatus = true;
  }
  changeStatusToSignIn(){
      this.SignInStatus = false;
  }
}
